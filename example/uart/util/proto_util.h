#ifndef _PROTO_UTIL_H_
#define _PROTO_UTIL_H_

#include <stdint.h>

typedef struct {
    uint16_t bit_idx;
    uint16_t byte_idx;
    const uint8_t *buf;
    uint16_t len;
} ctx_t;

void pop_arr_u8(uint8_t *dest, uint16_t len, ctx_t *ctx);

uint16_t pop_u8(ctx_t *ctx);

uint16_t pop_u16(ctx_t *ctx);

uint32_t pop_u32(ctx_t *ctx);

uint32_t pop_bits(uint8_t bit_count, ctx_t *ctx);

void put_arr_u8(uint8_t *src, uint16_t len, ctx_t *ctx);

void put_u8(uint8_t num, ctx_t *ctx);

void put_u16(uint16_t num, ctx_t *ctx);

void put_u32(uint32_t num, ctx_t *ctx);

void put_bits(uint32_t number, uint8_t bit_count, ctx_t *ctx);

#endif /* _PROTO_UTIL_H_ */
