#ifndef _UART_PROTOCOL_H_
#define _UART_PROTOCOL_H_

#include <stdint.h>
#include "onoff.h"

#include "dfu.h"


typedef enum {
        STATUS_OK = 0
        STATUS_FAIL = 1
} StatusCodes;

typedef struct {
        uint8_t msg_id;
        uint8_t RFU[2];
        union {
            DFUStartReq field_DFUStartReq
            DFUDoneReq field_DFUDoneReq
            DFUWriteReq field_DFUWriteReq
            OnReq field_OnReq
            OffReq field_OffReq
        } payload;
        uint16_t crc16;
} Request;

typedef struct {
        uint8_t msg_id;
        uint8_t RFU1[2];
        uint8_t status: 4;
        uint8_t RFU2: 4;
        union {
            DFUStartResp field_DFUStartResp
            DFUDoneResp field_DFUDoneResp
            DFUChunkResp field_DFUChunkResp
            OnResp field_OnResp
            OffResp field_OffResp
        } payload;
        uint16_t crc16;
} Response;


int parse_request(uint8_t *buf, uint16_t len, Request* request);
int encode_request(uint8_t *buf, uint16_t len, Request* request);
int parse_response(uint8_t *buf, uint16_t len, Response* response);
int encode_response(uint8_t *buf, uint16_t len, Response* response);


#endif /* _UART_PROTOCOL_H_ */