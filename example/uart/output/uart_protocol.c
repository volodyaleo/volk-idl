#include "uart_protocol.h"
#include "proto_util.h"


int parse_request(uint8_t *buf, uint16_t len, Request* request) {
    ctx_t ctx = {
        .bit_idx = 0,
        .byte_idx = 0,
        .buf = buf,
        .len = len
    };
    request->msg_id =pop_u8(&ctx);
    pop_arr_u8(request->RFU, 2, &ctx);
    switch(request->msg_id) {
        case 65:
            pop_arr_u8(request->payload.field_DFUStartReq.RFU, 16, &ctx);
            break;
        case 66:
            pop_arr_u8(request->payload.field_DFUDoneReq.RFU, 16, &ctx);
            break;
        case 68:
            request->payload.field_DFUWriteReq.addr = pop_u32(&ctx);
            pop_arr_u8(request->payload.field_DFUWriteReq.chunk, 12, &ctx);
            break;
        case 17:
            pop_arr_u8(request->payload.field_OnReq.RFU, 16, &ctx);
            break;
        case 16:
            pop_arr_u8(request->payload.field_OffReq.RFU, 16, &ctx);
            break;
    }
    request->crc16 =pop_u16(&ctx);

    return 0;
}

int encode_request(uint8_t *buf, uint16_t len, Request* request) {
    ctx_t ctx = {
        .bit_idx = 0,
        .byte_idx = 0,
        .buf = buf,
        .len = len
    };
    put_u8(request->msg_id , &ctx);
    put_arr_u8(RFU, 2, &ctx);
    switch(request->msg_id) {
        case 65:
            put_arr_u8(request->payload.field_DFUStartReq.RFU, 16, &ctx);
            break;
        case 66:
            put_arr_u8(request->payload.field_DFUDoneReq.RFU, 16, &ctx);
            break;
        case 68:
            put_u32(request->payload.field_DFUWriteReq.addr, &ctx);
            put_arr_u8(request->payload.field_DFUWriteReq.chunk, 12, &ctx);
            break;
        case 17:
            put_arr_u8(request->payload.field_OnReq.RFU, 16, &ctx);
            break;
        case 16:
            put_arr_u8(request->payload.field_OffReq.RFU, 16, &ctx);
            break;
    }
    put_u16(request->crc16 , &ctx);

    return 0;
}
int parse_response(uint8_t *buf, uint16_t len, Response* response) {
    ctx_t ctx = {
        .bit_idx = 0,
        .byte_idx = 0,
        .buf = buf,
        .len = len
    };
    response->msg_id =pop_u8(&ctx);
    pop_arr_u8(response->RFU1, 2, &ctx);
    response->status =pop_bits(4, &ctx);
    response->RFU2 =pop_bits(4, &ctx);
    switch(response->msg_id) {
        case 65:
            pop_arr_u8(response->payload.field_DFUStartResp.RFU, 15, &ctx);
            break;
        case 66:
            pop_arr_u8(response->payload.field_DFUDoneResp.RFU, 15, &ctx);
            break;
        case 68:
            response->payload.field_DFUChunkResp.rem_bytes = pop_u32(&ctx);
            pop_arr_u8(response->payload.field_DFUChunkResp.RFU, 11, &ctx);
            break;
        case 17:
            pop_arr_u8(response->payload.field_OnResp.RFU, 15, &ctx);
            break;
        case 16:
            pop_arr_u8(response->payload.field_OffResp.RFU, 15, &ctx);
            break;
    }
    response->crc16 =pop_u16(&ctx);

    return 0;
}

int encode_response(uint8_t *buf, uint16_t len, Response* response) {
    ctx_t ctx = {
        .bit_idx = 0,
        .byte_idx = 0,
        .buf = buf,
        .len = len
    };
    put_u8(response->msg_id , &ctx);
    put_arr_u8(RFU1, 2, &ctx);
    put_bits(response->status, 4, &ctx);
    put_bits(response->RFU2, 4, &ctx);
    switch(response->msg_id) {
        case 65:
            put_arr_u8(response->payload.field_DFUStartResp.RFU, 15, &ctx);
            break;
        case 66:
            put_arr_u8(response->payload.field_DFUDoneResp.RFU, 15, &ctx);
            break;
        case 68:
            put_u32(response->payload.field_DFUChunkResp.rem_bytes, &ctx);
            put_arr_u8(response->payload.field_DFUChunkResp.RFU, 11, &ctx);
            break;
        case 17:
            put_arr_u8(response->payload.field_OnResp.RFU, 15, &ctx);
            break;
        case 16:
            put_arr_u8(response->payload.field_OffResp.RFU, 15, &ctx);
            break;
    }
    put_u16(response->crc16 , &ctx);

    return 0;
}
