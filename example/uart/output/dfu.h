#ifndef _DFU_H_
#define _DFU_H_

#include <stdint.h>

typedef enum {
        DFUBase = 64
        DFUStart = 65
        DFUDone = 66
        DFUChunk = 68
} DFUOpcodes;

typedef struct {
        uint8_t RFU[16];
} DFUStartReq;

typedef struct {
        uint8_t RFU[16];
} DFUDoneReq;

typedef struct {
        uint32_t addr;
        uint8_t chunk[12];
} DFUWriteReq;

typedef struct {
        uint8_t RFU[15];
} DFUStartResp;

typedef struct {
        uint8_t RFU[15];
} DFUDoneResp;

typedef struct {
        uint32_t rem_bytes;
        uint8_t RFU[11];
} DFUChunkResp;



#endif /* _DFU_H_ */