#ifndef _ONOFF_H_
#define _ONOFF_H_

#include <stdint.h>

typedef enum {
        OnOffBase = 16
        Off = 16
        On = 17
} OnOffOpcodes;

typedef struct {
        uint8_t RFU[16];
} OnReq;

typedef struct {
        uint8_t RFU[16];
} OffReq;

typedef struct {
        uint8_t RFU[15];
} OnResp;

typedef struct {
        uint8_t RFU[15];
} OffResp;



#endif /* _ONOFF_H_ */