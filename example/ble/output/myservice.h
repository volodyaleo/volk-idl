#include <stdint.h>
#include "myservice_defs.h"

const char MyService_srv_uuid[] = "0x7421";
const char controlPoint_char_uuid[] = "0x5864";
const char data_char_uuid[] = "0x1234";

typedef struct {
        uint8_t opcode: 4;
        uint8_t RFU: 3;
        uint8_t is_response: 1;
        union {
            StartPayload field_StartPayload
            StopPayload field_StopPayload
        } args;
        uint8_t response_code;
} ControlPoint;

typedef struct {
        uint8_t payload[];
} Data;