#include <stdint.h>

typedef enum {
        OPCODE_START = 0
        OPCODE_END = 1
} OpCodes;

typedef enum {
        RESPONSE = 1
        REQUEST = 0
} IsResponse;

typedef enum {
        SUCCESS = 0
        ERROR = 1
} StopCodes;

typedef struct {
        uint16_t handle;
} StartPayload;

typedef struct {
        uint16_t handle;
        uint8_t stop_code;
} StopPayload;