from sly import Parser

from lang.lexer import VIDLLexer
from primitives.enumeration import Enum, EnumMember
from primitives.expr import Expr, Inverse, ExprGroup, Number, InverseBits
from primitives.iface import Interface, InterfaceMethod, InterfaceMethodArg, InterfaceMember
from primitives.imports import ImportedModule, ImportedSymbol
from primitives.statment import Statement
from primitives.struct import Struct, StructField
from primitives.symbol import NamedSymbol, Reference, Alias
from primitives.tags import Tag
from primitives.type import SymbolType, AliasType


def parse_number(num):
    if '0x' in num:
        return int(num, 16)
    if '.' in num:
        return float(num)
    return int(num)


class VIDLParser(Parser):
    tokens = VIDLLexer.tokens

    precedence = (
        ('left', PLUS, MINUS),
        ('left', TIMES, DIVIDE),
        ('right', UMINUS)
    )

    def __init__(self):
        self.names = {}

    ## Module items rules

    @_('term term')
    def term(self, p):
        t0 = p.term0
        t1 = p.term1
        t0.extend(t1)
        return t0

    @_('enum_def SEPARATOR')
    def term(self, p):
        return [p.enum_def]

    @_('statement SEPARATOR')
    def term(self, p):
        return [p.statement]

    @_('interface SEPARATOR')
    def term(self, p):
        return [p.interface]

    @_('struct SEPARATOR')
    def term(self, p):
        return [p.struct]

    @_('DECORATOR ALIAS NAME COLON expr struct SEPARATOR')
    def term(self, p):
        return [Alias(p.NAME, p.expr, p.struct), p.struct]

    # Module importing rules
    @_('IMPORT NAME SEPARATOR')
    def term(self, p):
        return [ImportedModule(p.NAME, p.NAME, lineno=p.lineno)]

    @_('IMPORT NAME AS NAME SEPARATOR')
    def term(self, p):
        return [ImportedModule(p.NAME0, p.NAME1, lineno=p.lineno)]

    @_('FROM NAME IMPORT import_list SEPARATOR')
    def term(self, p):
        return [ImportedSymbol(p.NAME, p.import_list, lineno=p.lineno)]

    @_('NAME')
    def import_list(self, p):
        return [{'alias': p.NAME, 'name': p.NAME, 'lineno': p.lineno}]

    @_('NAME AS NAME')
    def import_list(self, p):
        return [{'alias': p.NAME1, 'name': p.NAME0, 'lineno': p.lineno}]

    @_('import_list COMMA import_list')
    def import_list(self, p):
        i0 = p.import_list0
        i0.extend(p.import_list1)
        return i0

    ### Decorators

    @_('DECORATOR NAME COLON NUMBER SEPARATOR')
    def decorator_item(self, p):
        return [Tag(tag=p.NAME, value=parse_number(p.NUMBER), lineno=p.lineno)]

    @_('DECORATOR NAME COLON NAME SEPARATOR')
    def decorator_item(self, p):
        return [Tag(tag=p.NAME0, value=Reference(p.NAME1, lineno=p.lineno), lineno=p.lineno)]

    @_('DECORATOR NAME COLON expr SEPARATOR')
    def decorator_item(self, p):
        return [Tag(tag=p.NAME, value=p.expr, lineno=p.lineno)]

    @_('DECORATOR NAME COLON statement SEPARATOR')
    def decorator_item(self, p):
        return [Tag(tag=p.NAME, value=p.statement, lineno=p.lineno)]

    @_('DECORATOR NAME COLON STRING SEPARATOR')
    def decorator_item(self, p):
        return [Tag(tag=p.NAME, value=p.STRING.replace('"', ''), lineno=p.lineno)]

    @_('decorator_item decorator_item')
    def decorator_item(self, p):
        d1 = p.decorator_item0
        d1.extend(p.decorator_item1)
        return d1

    ### Type definition

    @_("NAME")
    def type_def(self, p):
        return SymbolType(p.NAME, lineno=p.lineno)

    @_("VOID")
    def type_def(self, p):
        return SymbolType('void', lineno=p.lineno)

    @_("NAME LSBRACE RSBRACE")
    def type_def(self, p):
        return SymbolType(p.NAME, is_array=True, lineno=p.lineno)

    @_("NAME LSBRACE NUMBER RSBRACE")
    def type_def(self, p):
        return SymbolType(p.NAME, is_array=True, array_len=p.NUMBER, lineno=p.lineno)

    @_("NAME LABRACE NAME RABRACE")
    def type_def(self, p):
        return AliasType(p.NAME0, p.NAME1, lineno=p.lineno)

    ### Interface rules

    @_('decorator_item INTERFACE NAME EXTENDS NAME LBRACE iface_items RBRACE')
    def interface(self, p):
        return Interface(p.NAME0, p.iface_items, lineno=p.lineno, tags=p.decorator_item, extends=p.NAME1)

    @_('decorator_item INTERFACE NAME LBRACE iface_items RBRACE')
    def interface(self, p):
        return Interface(p.NAME, p.iface_items, lineno=p.lineno, tags=p.decorator_item)

    @_('INTERFACE NAME EXTENDS NAME LBRACE iface_items RBRACE')
    def interface(self, p):
        return Interface(p.NAME0, p.iface_items, lineno=p.lineno, extends=NamedSymbol(p.NAME1, lineno=p.lineno))

    @_('INTERFACE NAME LBRACE iface_items RBRACE')
    def interface(self, p):
        return Interface(p.NAME, p.iface_items, lineno=p.lineno)

    @_('iface_items iface_items')
    def iface_items(self, p):
        it0 = p.iface_items0
        it1 = p.iface_items1
        it0.extend(it1)
        return it0

    @_('interface_method SEPARATOR')
    def iface_items(self, p):
        return [p.interface_method]

    @_('type_def NAME LPAREN interface_method_args RPAREN')
    def interface_method(self, p):
        return InterfaceMethod(p.NAME, p.type_def, p.interface_method_args, lineno=p.lineno)

    @_('decorator_item type_def NAME LPAREN interface_method_args RPAREN')
    def interface_method(self, p):
        return InterfaceMethod(p.NAME, p.type_def, p.interface_method_args, lineno=p.lineno,
                               tags=p.decorator_item)

    @_('interface_method_args COMMA interface_method_args')
    def interface_method_args(self, p):
        res = []
        if isinstance(p.interface_method_args0, list):
            res.extend(p.interface_method_args0)
        elif isinstance(p.interface_method_args0, InterfaceMethodArg):
            res.append(p.interface_method_args0)
        else:
            raise RuntimeError("Cannot parse line (wrong term): " + str(p.lineno))

        if isinstance(p.interface_method_args1, list):
            res.extend(p.interface_method_args1)
        elif isinstance(p.interface_method_args1, InterfaceMethodArg):
            res.append(p.interface_method_args1)
        else:
            raise RuntimeError("Cannot parse line (wrong term): " + str(p.lineno))

        return res

    @_('type_def NAME')
    def interface_method_args(self, p):
        return [InterfaceMethodArg(p.type_def, p.NAME, lineno=p.lineno)]

    @_('VOID')
    def interface_method_args(self, p):
        return []

    @_('decorator_item type_def NAME SEPARATOR')
    def iface_items(self, p):
        return [InterfaceMember(p.type_def, p.NAME, lineno=p.lineno, tags=p.decorator_item)]

    @_('NAME NAME SEPARATOR')
    def iface_items(self, p):
        return [InterfaceMember(SymbolType(p.NAME0, lineno=p.lineno), p.NAME1, lineno=p.lineno)]

    @_('enum_def SEPARATOR')
    def iface_items(self, p):
        return [p.enum_def]

    @_('struct SEPARATOR')
    def iface_items(self, p):
        return [p.struct]

    ### Structure rules

    @_('STRUCT NAME LBRACE struct_items RBRACE')
    def struct(self, p):
        return Struct(p.NAME, p.struct_items, lineno=p.lineno)

    @_('decorator_item STRUCT NAME LBRACE struct_items RBRACE')
    def struct(self, p):
        return Struct(p.NAME, p.struct_items, lineno=p.lineno, tags=p.decorator_item)

    @_('struct_items struct_items')
    def struct_items(self, p):
        si0 = p.struct_items0
        si0.extend(p.struct_items1)
        return si0

    @_('type_def NAME SEPARATOR')
    def struct_items(self, p):
        return [StructField(p.type_def, p.NAME, lineno=p.lineno)]

    @_('type_def NAME COLON NUMBER SEPARATOR')
    def struct_items(self, p):
        return [StructField(p.type_def, p.NAME, bitsize=p.NUMBER, lineno=p.lineno)]

    @_('decorator_item type_def NAME SEPARATOR')
    def struct_items(self, p):
        return [StructField(p.type_def, p.NAME, lineno=p.lineno, tags=p.decorator_item)]

    @_('decorator_item type_def NAME COLON NUMBER SEPARATOR')
    def struct_items(self, p):
        return [StructField(p.type_def, p.NAME, bitsize=p.NUMBER, lineno=p.lineno, tags=p.decorator_item)]

    ### Enum rules

    @_('ENUM NAME LBRACE enum_items RBRACE')
    def enum_def(self, p):
        return Enum(p.NAME, p.enum_items, lineno=p.lineno)

    @_('decorator_item ENUM NAME LBRACE enum_items RBRACE')
    def enum_def(self, p):
        return Enum(p.NAME, p.enum_items, lineno=p.lineno, tags=p.decorator_item)

    @_('enum_items COMMA enum_items')
    def enum_items(self, p):
        it0 = p.enum_items0
        it1 = p.enum_items1

        if not isinstance(it0, list):
            raise RuntimeError("Cannot parse line (wrong term): " + str(p.lineno))

        if not isinstance(it1, list):
            raise RuntimeError("Cannot parse line (wrong term): " + str(p.lineno))

        res = []
        res.extend(it0)
        res.extend(it1)

        return res

    @_('statement')
    def enum_items(self, p):
        s = p.statement
        return [EnumMember(name=s.name, value=s.expr, lineno=s.lineno)]

    @_('decorator_item statement')
    def enum_items(self, p):
        s = p.statement
        return [EnumMember(name=s.name, value=s.expr, tags=p.decorator_item, lineno=s.lineno)]

    ### Expressions rules

    @_('NAME ASSIGN expr')
    def statement(self, p):
        return Statement(p.NAME, p.expr, lineno=p.lineno)

    @_('expr PLUS expr')
    def expr(self, p):
        return Expr('+', p.expr0, p.expr1, lineno=p.lineno)

    @_('expr MINUS expr')
    def expr(self, p):
        return Expr('-', p.expr0, p.expr1, lineno=p.lineno)

    @_('expr TIMES expr')
    def expr(self, p):
        return Expr('*', p.expr0, p.expr1, lineno=p.lineno)

    @_('expr DIVIDE expr')
    def expr(self, p):
        return Expr('/', p.expr0, p.expr1, lineno=p.lineno)

    @_('expr AND expr')
    def expr(self, p):
        return Expr('&', p.expr0, p.expr1, lineno=p.lineno)

    @_('expr OR expr')
    def expr(self, p):
        return Expr('|', p.expr0, p.expr1, lineno=p.lineno)

    @_('expr XOR expr')
    def expr(self, p):
        return Expr('^', p.expr0, p.expr1, lineno=p.lineno)

    @_('REVBITS expr')
    def expr(self, p):
        return InverseBits(p.expr, lineno=p.lineno)

    @_('MINUS expr %prec UMINUS')
    def expr(self, p):
        return Inverse(p.expr, lineno=p.lineno)

    @_('LPAREN expr RPAREN')
    def expr(self, p):
        return ExprGroup(p.expr, lineno=p.lineno)

    @_('NUMBER')
    def expr(self, p):
        return Number(parse_number(p.NUMBER), lineno=p.lineno)

    @_('NAME')
    def expr(self, p):
        return Reference(p.NAME, lineno=p.lineno)

    def error(self, p):
        if p:
            raise RuntimeError("Syntax error at token " + str(p.type) + " at line: " + str(p.lineno) +
                               " file: " + self._filename)
        else:
            raise RuntimeError("Syntax error at EOF in file: " + self._filename)

    def set_filename(self, name):
        self._filename = name
