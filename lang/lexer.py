from sly import Lexer


class VIDLLexer(Lexer):
    tokens = {
        NAME, NUMBER, STRING,
        PLUS, TIMES, MINUS, DIVIDE, ASSIGN,
        OR, AND, XOR, REVBITS,
        LPAREN, RPAREN, LBRACE, RBRACE, LSBRACE, RSBRACE,
        LABRACE, RABRACE,
        ENUM, INTERFACE, STRUCT,
        COMMA, SEPARATOR, VOID, EXTENDS, DECORATOR, COLON,
        IMPORT, AS, FROM, ALIAS
    }
    ignore = ' \t'

    # Tokens
    NAME = r'[a-zA-Z_][a-zA-Z0-9_\.]*'
    NUMBER = r'-?(0x)?\d+(\.\d+)?'

    # Ariphmetic symbols
    PLUS = r'\+'
    MINUS = r'-'
    TIMES = r'\*'
    DIVIDE = r'/'

    # Bits operators
    OR = r'\|'
    AND = r'\&'
    XOR = r'\^'
    REVBITS = r'\~'

    ASSIGN = r'='
    LPAREN = r'\('
    RPAREN = r'\)'
    LBRACE = r'\{'
    RBRACE = r'\}'
    LSBRACE = r'\['
    RSBRACE = r'\]'
    LABRACE = r'\<'
    RABRACE = r'\>'
    NAME['enum'] = ENUM
    NAME['interface'] = INTERFACE
    NAME['struct'] = STRUCT
    NAME['void'] = VOID
    NAME['extends'] = EXTENDS
    NAME['alias'] = ALIAS
    COMMA = r','
    SEPARATOR = r';'

    DECORATOR = r'@'
    COLON = r':'

    NAME['import'] = IMPORT
    NAME['as'] = AS
    NAME['from'] = FROM

    STRING = r'"[^"]*"'

    # Ignored pattern
    ignore_newline = r'\n+'

    # Extra action for newlines
    def ignore_newline(self, t):
        self.lineno += t.value.count('\n')

    def error(self, t):
        raise RuntimeError("Illegal character '%s'" % t.value[0] + " at file " + self._filename)

    def set_filename(self, name):
        self._filename = name
