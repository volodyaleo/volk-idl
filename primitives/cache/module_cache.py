def singleton(class_):
    instances = {}
    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return getinstance


@singleton
class ModulesCache:

    def __init__(self):
        self.files = {}

    def add(self, file, module):
        self.files[file.upper()] = module

    def get(self, file):
        return self.files.get(file.upper(), None)