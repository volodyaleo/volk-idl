from primitives.symbol import Solvable


class Tag(Solvable):

    def __init__(self, tag, value, lineno):
        self.tag = tag
        self.value = value
        self.lineno = lineno

    def solve(self, scopes: list):
        if isinstance(self.value, Solvable):
            ret = self.value.solve(scopes=scopes)
            if ret is not None:
                self.value = ret

    def __str__(self):
        return "<Tag: " + self.tag + ":" + str(self.value) +  ">"
