from primitives.symbol import NamedSymbol, Container, Solvable
from primitives.tags import Tag
from primitives.utils import listify


class Struct(NamedSymbol, Container, Solvable):

    def __init__(self, name, items=[], lineno=0, tags=[]):
        NamedSymbol.__init__(self, name=name, lineno=lineno)
        Container.__init__(self, items=items + tags)

    @property
    @listify
    def tags(self):
        for i in self.items:
            if isinstance(i, Tag):
                yield i

    def find_tag_by_name(self, name):
        for i in self.items:
            if isinstance(i, Tag):
                if i.tag == name:
                    return i

    @property
    @listify
    def members(self):
        for i in self.items:
            if isinstance(i, StructField):
                yield i

    def copy(self):
        n = Struct(self.name)
        n.items = self.items
        return n

    def solve(self, scopes: list):
        scopes = scopes + [self]
        for i in self.items:
            if isinstance(i, Solvable):
                i.solve(scopes=scopes)


class StructField(NamedSymbol, Container):

    def __init__(self, type, name, bitsize=None, lineno=0, tags=[]):
        NamedSymbol.__init__(self, name=name, lineno=lineno)
        Container.__init__(self, tags)
        self.type = type
        self.bitsize=bitsize

    @property
    @listify
    def tags(self):
        for i in self.items:
            if isinstance(i, Tag):
                yield i

    def find_tag_by_name(self, name):
        for i in self.items:
            if isinstance(i, Tag):
                if i.tag == name:
                    return i

    def solve(self, scopes: list):
        Container.solve(self, scopes)
        self.type.solve(scopes=scopes)
