from primitives.expr import Expr, Number
from primitives.iface import Interface
from primitives.enumeration import Enum, EnumMember
from primitives.imports import ImportedModule, ImportedSymbol
from primitives.struct import Struct
from primitives.symbol import NamedSymbol, Container, Solvable, Alias
from primitives.type import SymbolType
from primitives.utils import dictify, listify, unique_list
from .cache.module_cache import ModulesCache


cache = ModulesCache()


class Module:

    def __init__(self, name, fs):
        self.name = name
        self.items = []
        self.imported_items = []
        self.filesystem = fs

    @property
    @listify
    def enums(self):
        for i in self.items:
            if isinstance(i, Enum):
                yield i

    @property
    @listify
    def structs(self):
        for i in self.items:
            if isinstance(i, Struct):
                yield i

    @property
    @listify
    def interfaces(self):
        for i in self.items:
            if isinstance(i, Interface):
                yield i

    @property
    @listify
    def imports(self):
        for i in self.items:
            if isinstance(i, ImportedModule):
                yield i

    @property
    @listify
    def symbol_imports(self):
        for i in self.items:
            if isinstance(i, ImportedSymbol):
                yield i

    @property
    @unique_list
    def unique_imported_files_list(self):
        for i in self.items:
            if isinstance(i, ImportedModule):
                yield i.file
            if isinstance(i, ImportedSymbol):
                yield i.file

    @property
    @listify
    def external_ifaces(self):
        for i in self.imported_items:
            if isinstance(i, Interface):
                yield i

    @property
    @listify
    def external_struct(self):
        for i in self.imported_items:
            if isinstance(i, Struct):
                yield i

    @property
    @listify
    def external_enum(self):
        for i in self.imported_items:
            if isinstance(i, Enum):
                yield i

    @staticmethod
    def load_from_file(name, parser, lexer, filesystem):
        filename = filesystem.resolve_module_name(name)
        if filename is None:
            raise RuntimeError("Cannot find module: " + name)
        m = cache.get(filename)
        if m is None:
            with open(filename, "r") as f:
                text = f.read()

            parser.set_filename(filename)
            lexer.set_filename(filename)

            symlist = parser.parse(lexer.tokenize(text))
            m = Module(name, filesystem)
            m.add_symbols(symlist)
            m.resolve_imports(parser, lexer)
            m.solve()
            cache.add(filename, m)

        return m

    def add_symbol(self, symbol):
        """
        This function chooses the dictionary to store a symbol by symbol type
        :param symbol:
        :return:
        """
        self.items.append(symbol)

    def resolve_imports(self, parser, lexer):
        """
        Function for import modules and symbols
        :param parser:
        :param lexer:
        :return:
        """
        for imp in self.items:
            if isinstance(imp, ImportedModule):
                imp.module = self.load_from_file(imp.file, parser, lexer, self.filesystem)

        for ext in self.items:
            if isinstance(ext, ImportedSymbol):
                m = self.load_from_file(ext.file, parser, lexer, self.filesystem)

                for sym in ext.symbols:
                    name = sym['name']
                    alias = sym['alias'] # New symbol name

                    s = m.find_symbol(name)
                    if s is None:
                        symbols = m.resolve_alias(name)
                        if not len(symbols):
                            raise Exception("Symbol " + str(name) + " is not found in " + ext.file)

                        def copy_sym(original):
                            copied = original.copy()
                            copied.name = alias
                            return copied

                        self.imported_items.extend(list(map(copy_sym, symbols)))
                    else:
                        s = s.copy()
                        s.name = alias
                        self.imported_items.append(s)

    def add_symbols(self, moduleItems):
        self.items.extend(moduleItems)

    def find_imported_module(self, name: str):
        for imp in self.items:
            if isinstance(imp, ImportedModule):
                if name.startswith(imp.name):
                    next_name = name[len(imp.name) + 1:]

                    return imp, next_name
        return None

    def find_symbol(self, name: str, scope=None):
        """
        Function for symbol searching
        :param name: list of symbols (location). For example: ['module2', 'iface3', 'enum5']
        :return:
        """
        assert isinstance(name, str)
        _tmp = name.split('.')
        search_name = _tmp[0]
        if len(_tmp) > 1:
            next_name = '.'.join(_tmp[1:])
        else:
            next_name = None

        if scope is not None:
            if hasattr(scope, 'find_symbol'):
                sym = scope.find_symbol(name)
                if sym is not None:
                    return sym

        for i in self.items + self.imported_items:
            if not isinstance(i, (ImportedModule, ImportedSymbol, Alias)):
                if i.name == search_name:
                    sym = i

                    if next_name and isinstance(sym, Container):
                        return sym.find_symbol(next_name)
                    else:
                        return sym

        # Search in imports
        res = self.find_imported_module(name)
        if res is not None:
            sym, relsym = res
            return sym.module.find_symbol(relsym)

        return None

    @listify
    def resolve_alias(self, alias: str):
        assert isinstance(alias, str)

        for i in self.items + self.imported_items:
            if isinstance(i, Alias):
                if i.alias == alias:
                    yield i

    def solve(self):
        """
        Function for solve all constant expressions
        :return:
        """
        for sym in self.items:
            if isinstance(sym, Solvable):
                sym.solve(scopes=[self])

    def __str__(self):
        return "<Module>"
