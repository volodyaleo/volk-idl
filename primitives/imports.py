

class ImportedModule:
    """
    rules:
        IMPORT NAME SEPARATOR
        IMPORT NAME AS NAME SEPARATOR
    """
    def __init__(self, file, name, lineno):
        self.file = file
        self.name = name
        self.module = None
        self.lineno = lineno

    def initialize(self, module):
        self.module = module


class ImportedSymbol:
    """
    Imported symbol from external module.
    This type of imports creates clone of symbol form external module with defined name

    rules:
        FROM NAME IMPORT NAME [AS NAME]+
    """

    def __init__(self, file, symbols, lineno):
        self.file = file
        self.symbols = symbols
        self.lineno = lineno
