class Solvable:

    def solve(self, scopes: list):
        raise Exception("Not implemented")


class NamedSymbol:
    """
    Link on another object.
    Example:
        ["Interface1", "MyEnum2", "MyField1"]
    """
    def __init__(self, name, lineno):
        self.name = name
        self.lineno = lineno

    def __str__(self):
        return "<" + self.__class__.__name__ + ": " + self.name + ">"


class Container(Solvable):

    def __init__(self, items):
        self.items = items

    def find_symbol(self, name, root=None, scope=None):
        if scope is not None:
            assert isinstance(scope, Container)

        _tmp = name.split('.')

        name_to_find = _tmp[0]
        if len(_tmp) > 1:
            next_name = '.'.join(_tmp[1:])
        else:
            next_name = None

        if scope:
            sym = scope.find_symbol(name=name, root=root)
            if sym is not None:
                return sym

        for i in self.items:
            if isinstance(i, NamedSymbol):
                if i.name == name_to_find:
                    if next_name is not None and isinstance(i, Container):
                        return i.find_symbol(next_name)
                    elif next_name is None:
                        return i
                    else:
                        break

        if root is not None:
            return root.find_symbol(name)

        return None

    def solve(self, scopes: list):
        """
        Function for solve all const expressions and enum values
        :param scopes: list of parent symbols-containers
        :return:
        """
        for sym in self.items:  # For each enum
            if isinstance(sym, Solvable):
                sym.solve(scopes=scopes + [self])


class Reference(NamedSymbol, Solvable):

    def solve(self, scopes: list):
        for s in scopes:
            sym = s.find_symbol(self.name)
            if sym is not None:
                if isinstance(sym, Solvable):
                    ret = sym.solve(scopes=scopes)
                    if ret is not None:
                        return ret
                return sym
        raise Exception("Cannot find symbol: " + self.name)


class Alias(Solvable):

    def __init__(self, alias, alias_num, destination):
        self.alias = alias
        self.alias_num = alias_num
        self.destination = destination

    def solve(self, scopes):
        if isinstance(self.alias_num, Solvable):
            self.alias_num = self.alias_num.solve(scopes)

    def copy(self):
        return Alias(self.alias, self.alias_num, self.destination)
