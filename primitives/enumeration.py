from primitives.symbol import NamedSymbol, Container, Solvable
from primitives.tags import Tag
from primitives.utils import listify


class EnumMember(NamedSymbol, Container):

    def __init__(self, name, value, tags=[], lineno=0):
        super().__init__(name=name, lineno=lineno)
        self.value = value
        Container.__init__(self, tags)

    @property
    def tags(self):
        return self.items

    def find_tag_by_name(self, name):
        for i in self.items:
            if isinstance(i, Tag):
                if i.tag == name:
                    return i

    def solve(self, scopes):
        Container.solve(self, scopes=scopes)

        if isinstance(self.value, Solvable):
            ret = self.value.solve(scopes=scopes)
            if ret is not None:
                self.value = ret
                return ret
            else:
                raise Exception("Unexpected error with enum member: " + self.name + " at line: " + str(self.lineno))
        elif isinstance(self.value, int):
            return self.value
        elif isinstance(self.value, float):
            return self.value

        raise Exception("Unexpected value type for: " + self.name + " type: " + str(type(self.value)) + " at line: " + str(self.lineno))


class Enum(NamedSymbol, Container):

    def __init__(self, name, members=[], lineno=0, tags=[]):
        NamedSymbol.__init__(self, name=name, lineno=lineno)
        Container.__init__(self, items=members + tags)

    @property
    @listify
    def tags(self):
        for i in self.items:
            if isinstance(i, Tag):
                yield i

    @property
    @listify
    def members(self):
        for i in self.items:
            if isinstance(i, EnumMember):
                yield i

    def copy(self):
        n = Enum(self.name)
        n.items = self.items
        return n
