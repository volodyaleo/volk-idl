from primitives.enumeration import EnumMember
from primitives.symbol import NamedSymbol, Solvable


class Expr(Solvable):

    def __init__(self, operator, operand1, operand2, lineno):
        self.operator = operator
        self.operand1 = operand1
        self.operand2 = operand2
        self.lineno = lineno

    def _extract_operand(self, op, scopes):
        op1 = None

        if isinstance(op, Expr):
            op1 = op.solve(scopes=scopes)
        elif isinstance(op, NamedSymbol):
            for scope in scopes:
                op1 = scope.find_symbol(op.name)
                if op1 is not None:
                    break

            if isinstance(op1, Solvable):
                op1 = op1.solve(scopes)
        else:
            raise Exception("Unknown operand")

        if op1 is None:
            raise Exception("Cannot find symbol: " + str(op.name) + " scope: " + scope.name + " symbol type: " + str(type(op)))

        return op1

    def solve(self, scopes):
        op1 = self._extract_operand(self.operand1, scopes)
        op2 = self._extract_operand(self.operand2, scopes)

        if self.operator == '+':
            return op1 + op2
        elif self.operator == '*':
            return op1 * op2
        elif self.operator == '/':
            return op1 / op2
        elif self.operator == '-':
            return op1 - op2
        elif self.operator == '&':
            return op1 & op2
        elif self.operator == '|':
            return op1 | op2
        elif self.operator == '^':
            return op1 ^ op2
        else:
            raise Exception("Unknown operator: " + self.operator)

    def __str__(self):
        return str(self.operand1) + self.operator + str(self.operand2)


class Inverse(Expr):

    def __init__(self, val, lineno):
        self.operand1 = val
        self.lineno = lineno

    @property
    def args(self):
        return [self.operand1]

    def solve(self, scopes):
        op1 = self._extract_operand(self.operand1, scopes)

        return -op1

    def __str__(self):
        return "-" + str(self.operand1)


class InverseBits(Expr):

    def __init__(self, val, lineno):
        self.operand1 = val
        self.lineno = lineno

    @property
    def args(self):
        return [self.operand1]

    def solve(self, scopes):
        op1 = self._extract_operand(self.operand1, scopes)

        return ~op1

    def __str__(self):
        return "~" + str(self.operand1)


class ExprGroup(Expr):

    def __init__(self, expr, lineno):
        self.expr = expr
        self.lineno = lineno

    def solve(self, scopes):
        return self._extract_operand(self.expr, scopes)

    def __str__(self):
        return "(" + str(self.expr) + ")"


class Number(Expr):

    def __init__(self, val, lineno):
        self.value = val
        self.lineno = lineno

    def solve(self, scopes):
        return self.value

    def __str__(self):
        return str(self.value)