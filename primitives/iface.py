from primitives.expr import Expr
from primitives.struct import Struct
from primitives.symbol import NamedSymbol, Container, Solvable, Reference
from primitives.tags import Tag
from primitives.type import SymbolType
from primitives.utils import dictify, listify
from .enumeration import Enum, EnumMember


class InterfaceExtender(Reference):
    def __init__(self, name, lineno):
        Reference.__init__(self, name=name, lineno=lineno)
        self.reference = None

    def solve(self, scopes: list):
        self.reference = Reference.solve(self, scopes=scopes)


class InterfaceMethodArg(Solvable):

    def __init__(self, type, name, lineno):
        self.type = type
        self.name = name
        self.lineno = lineno

    def solve(self, scopes: list):
        self.type.solve(scopes=scopes)


class InterfaceMethod(NamedSymbol, Container):
    def __init__(self, name, return_type, args, lineno, tags=[]):
        NamedSymbol.__init__(self, name=name, lineno=lineno)
        Container.__init__(self, tags + args + [return_type])

    @property
    @listify
    def tags(self):
        for i in self.items:
            if isinstance(i, Tag):
                yield i

    @property
    @listify
    def args(self):
        for i in self.items:
            if isinstance(i, InterfaceMethodArg):
                yield i

    @property
    def return_type(self):
        for i in self.items:
            if isinstance(i, SymbolType):
                return i


class InterfaceMember(NamedSymbol, Solvable):
    def __init__(self, type, name, lineno, tags=[]):
        super().__init__(name=name, lineno=lineno)
        self.type = type
        self.tags = tags

    def find_tag_by_name(self, name):
        for i in self.tags:
            if i.tag == name:
                return i

    def solve(self, scopes: list):
        self.type.solve(scopes=scopes)

    def __str__(self):
        return str(self.type) + " " + self.name


class Interface(NamedSymbol, Container):
    def __init__(self, name, items=None, lineno=0, tags=[], extends=None):
        NamedSymbol.__init__(self, name=name, lineno=lineno)

        ext = []
        if extends is not None:
            ext = [InterfaceExtender(extends, lineno=lineno)]

        Container.__init__(self, items=items + ext + tags)

    @property
    def extends(self):
        for i in self.items:
            if isinstance(i, InterfaceExtender):
                return i.reference
        return None

    @property
    @listify
    def tags(self):
        for i in self.items:
            if isinstance(i, Tag):
                yield i

    def find_tag_by_name(self, name):
        for i in self.items:
            if isinstance(i, Tag):
                if i.tag == name:
                    return i

    @property
    @listify
    def enums(self):
        for i in self.items:
            if isinstance(i, Enum):
                yield i

    @property
    @listify
    def structs(self):
        for i in self.items:
            if isinstance(i, Struct):
                yield i

    @property
    @listify
    def members(self):
        for i in self.items:
            if isinstance(i, InterfaceMember):
                yield i

    @property
    @listify
    def methods(self):
        for i in self.items:
            if isinstance(i, InterfaceMethod):
                yield i

    def copy(self):
        return Interface(self.name, self.items)
