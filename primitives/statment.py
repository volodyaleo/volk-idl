

class Statement:
    """
    Rule:
        NAME = EXPR
    """

    def __init__(self, name, expr, lineno):
        self.name = name
        self.expr = expr
        self.lineno = lineno

    def __str__(self):
        return self.name + " = " + str(self.expr)
