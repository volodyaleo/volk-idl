from primitives.symbol import Solvable


class DefaultType:
    _name = "Unknown"

    def __str__(self):
        return self.name


class VoidType(DefaultType):
    _name = "void"


class IntType(DefaultType):
    _name = "int"


class UIntType(DefaultType):
    _name = "uint"


class U8Type(DefaultType):
    _name = "u8"


class I8Type(DefaultType):
    _name = "i8"


class U16Type(DefaultType):
    _name = "u16"


class I16Type(DefaultType):
    _name = "i16"


class U32Type(DefaultType):
    _name = "u32"


class I32Type(DefaultType):
    _name = "i32"


class StringType(DefaultType):
    _name = "str"


class RealType(DefaultType):
    _name = 'real'


default_types = {
    IntType._name: IntType,
    StringType._name: StringType,
    RealType._name: RealType,
    VoidType._name: VoidType,
    UIntType._name: UIntType,
    U8Type._name: U8Type,
    I8Type._name: I8Type,
    U16Type._name: U16Type,
    I16Type._name: I16Type,
    U32Type._name: U32Type,
    I32Type._name: I32Type
}


class SymbolType(Solvable):
    """
    Type - primitive (uint, int, real, str) or enum or struct.
    Example:
        ["Interface1", "MyEnum2"]
        int
        str
    """
    _PRIMITIVE_ALIAS = {}

    def __init__(self, name, lineno, is_array=False, array_len=None):
        self._name = name
        self.lineno = lineno
        self.reference = default_types.get(name, None)
        self.primitive = self.reference is not None
        self.is_array = is_array
        self.array_length = array_len

    @property
    def name(self):
        name = self.item_type

        if self.is_array:
            if self.array_length is not None:
                return name + "[" + str(self.array_length) + "]"
            else:
                return name + "[]"
        else:
            return name

    @property
    def item_type(self):
        name = self._name

        if self.primitive:
            alias = SymbolType._PRIMITIVE_ALIAS.get(name, None)
            if alias is not None:
                name = alias
        return name

    def __str__(self):
        if self.reference:
            return str(self.reference)
        else:
            return "Unknown type: " + '.'.join(self.name)

    def solve(self, scopes: list):
        if self.reference is not None:
            return

        for s in scopes:
            sym = s.find_symbol(self.name)
            if sym is not None:
                self.reference = sym
                return

        raise Exception("cannot find type: " + str(self.name) + " line: " + str(self.lineno))

    @staticmethod
    def set_primitive_alias(aliases):
        SymbolType._PRIMITIVE_ALIAS = aliases


class AliasType(SymbolType):

    def __init__(self, alias, dependency_name, lineno):
        self.alias = alias
        self._dependency_name = dependency_name
        self.lineno = lineno
        self.is_alias = True
        self.dependency = None
        self.aliased_structures = []

    @property
    def name(self):
        return self.alias + "<" + self.dependency.name + ">"

    def solve(self, scopes):
        for s in scopes:
            sym = s.find_symbol(self._dependency_name)
            if sym is not None:
                self.dependency = sym
                break

        for s in scopes:
            if hasattr(s, "resolve_alias"):
                self.aliased_structures += s.resolve_alias(self.alias)

        if self.dependency is None:
            raise Exception("cannot find field: " + str(self._dependency_name) + " line: " + str(self.lineno))

    def __str__(self):
        return self.alias + "<" + self.dependency.name + ">"
