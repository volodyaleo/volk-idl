import os
from os import path


class FileSystem:

    def __init__(self, pwd=None, include_dirs=[], extension='.idl'):
        if pwd is None:
            pwd = os.getcwd()

        self.pwd = pwd
        self.include_dirs = include_dirs
        self.extension = extension

    def resolve_module_name(self, module_name):
        file = os.path.join(self.pwd, module_name + self.extension)

        if path.isfile(file):
            return file

        for inc in self.include_dirs:
            file = os.path.join(inc, module_name + self.extension)

            if path.isfile(file):
                return file

        return None
