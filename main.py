import argparse
import os

import jinja2
import yaml

from filesystem import FileSystem
from lang.lexer import VIDLLexer
from lang.parser import VIDLParser
from primitives.module import Module
from primitives.type import SymbolType


def module_loader(parser, lexer, fs):
    def load(name):
        return Module.load_from_file(name, parser, lexer, fs)
    return load


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Code generator')
    parser.add_argument('--project', dest='proj', type=str, help='project file (yaml)', required=True)
    args = parser.parse_args()

    lexer = VIDLLexer()
    parser = VIDLParser()

    with open(args.proj, 'r') as f:
        project = yaml.safe_load(f.read())

    for settings_name, settings in project.items():
        if settings_name.startswith("."):
            continue

        if 'pwd' not in settings:
            settings['pwd'] = os.path.dirname(args.proj)

        if 'out_ext' not in settings:
            settings['out_ext'] = '.gen'

        if 'imports' not in settings:
            settings['imports'] = []

        if 'templates' not in settings:
            settings['templates'] = settings['pwd']

        if 'output' not in settings:
            settings['output'] = settings['pwd']

        fs = FileSystem(pwd=settings['pwd'], include_dirs=settings['imports'])

        file_loader = jinja2.FileSystemLoader(settings['templates'])
        env = jinja2.Environment(loader=file_loader)
        template = env.get_template(settings['template'])

        modules = settings['modules']
        output_folder = settings['output']

        if 'type_aliases' in settings:
            SymbolType.set_primitive_alias(settings['type_aliases'])
        else:
            SymbolType.set_primitive_alias({})

        for g in map(module_loader(parser, lexer, fs), modules):
            with open(os.path.join(output_folder, g.name + settings["out_ext"]), 'w') as f:
                output = template.render(module=g)
                f.write(output)
                f.close()
