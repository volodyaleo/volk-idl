# Introduction

This repository contains the code generator for volk-idl syntax.

The IDL syntax is based on QFace.

Sly decided because it gives simple API for lexer and parser.

Features:
- preprocessor calculations for enumerators.
- import symbol from module
- searching symbol references
- structures aliasing

# Requirements

To install requirements enter the command bellow
```bash
pip install -r ./requirements.txt
```

# How run example

```bash
cd examples
cd uart
python3 ../../main.py --proj ./settings.yml
```
